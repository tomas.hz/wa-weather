import requests
import urllib.parse
import werkzeug.exceptions

import flask


weather_blueprint = flask.Blueprint(
	"weather",
	__name__
)


@weather_blueprint.route("/")
def get_weather():
	# Clean untrusted input
	location = urllib.parse.quote(
		(
			flask.request.args.get(
				"location",
				flask.request.cookies.get(
					"location",
					flask.current_app.config["LOCATION_DEFAULT"]
				)
			)
		),
		safe=""
	)
	
	if location == "":
		location = flask.current_app.config["LOCATION_DEFAULT"]

	weather_data = requests.get(
		"https://api.openweathermap.org/data/2.5/weather?q="
		f"{location}&"
		f"APPID={flask.current_app.config['API_KEY']}&"
		"units=metric"
	)

	attempt = 0
	error_occurred = False

	while not weather_data.ok:
		error_occurred = True
		
		if attempt == 3:
			response = flask.make_response(
				"An error occurred while fetching the weather data."
			)

			response.set_cookie("location", flask.current_app.config["LOCATION_DEFAULT"])

			return response, 500

		location = flask.current_app.config["LOCATION_DEFAULT"]
		weather_data = requests.get(
			"https://api.openweathermap.org/data/2.5/weather?q="
			f"{location}&"
			f"APPID={flask.current_app.config['API_KEY']}&"
			"units=metric"
		)

		attempt += 1

	response = flask.make_response(
		flask.render_template(
			"weather.html",
			data=weather_data.json()
		)
	)

	if flask.request.args.get("location") is not None and not error_occurred:
		response.set_cookie("location", flask.request.args["location"])

	return response
